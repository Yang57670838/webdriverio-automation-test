Feature: The Submit History Details Page

    @local
    Scenario Outline: As a user, I can see correct contents in the Submit History Details Page

        Given I am on the historyDetails page
        And I pause for 3000 ms
        Then I should see 5 rows in the historyDetails page grid table
@dashboard
Feature: The Dashboard page

    Scenario Outline: As a user, I can log into the dashboard and see left menu bar

        Given I am on the dashboard page
        And I pause for 3000 ms
        Then I should see the menu item <menuIndex> with text <menuTxt>

        Examples:
            | menuIndex | menuTxt                     |
            | 0         | Summary                     |
            | 1         | Cash Expense                |
            | 2         | Company Expense             |
            | 3         | Employee Salary             |
            | 4         | Personal Creditcard Expense |
            | 5         | Income                      |
            | 6         | Bank Statement              |
            | 7         | Submit GST Report           |
            | 8         | Submit Extra Document       |

import Page from './page.js';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class HistoryDetailsPage extends Page {
    /**
     * define selectors using getter methods
     */
    public get tableRows () {
        return $$('//table/tbody/tr');
    }

    public get tableHeaders () {
        return $$('//table/thead/tr/th');
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    public open () {
        return super.open('history-details');
    }
}

export default new HistoryDetailsPage();

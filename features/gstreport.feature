@gstSubmit
Feature: The GST Report page

    Scenario Outline: As a user, I can submit GST report in GST Report page

        Given I am on the gstreport page
        And I pause for 3000 ms
        Then I see page header as GST REPORT
        When I enter 12345 into text field #gstNotes with method add
        And I check the page checkbox
        And I left click on field .MuiSelect-select
        Then I should see the options inside current opening dropdown select
        | Option |
        | 2021-2022 QTR 1 |
        | 2021-2022 QTR 2 |
        | 2021-2022 QTR 3 |
        | 2021-2022 QTR 4 |
        | 2022-2023 QTR 1 |
        | 2022-2023 QTR 2 |
        | 2022-2023 QTR 3 |
        | 2022-2023 QTR 4 |
export default async (value: string, selector: string, method: "add" | "set") => {
    let ele = await $(selector);
    await ele.waitForDisplayed({ timeout: 5000 })
    if (method === "set") {
        await ele.setValue(value);
    } else {
        await ele.addValue(value);
    }
    // await browser.pause(30000);
}
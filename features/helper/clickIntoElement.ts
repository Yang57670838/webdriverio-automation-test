export default async (type: "left" | "middle" | "right", selector: string) => {
  let ele = await $(selector);
  await ele.waitForClickable({timeout: 5000})
  let clickType;
  switch (type) {
    case "left":
      clickType = 0;
      break;
    case "middle":
      clickType = 1;
      break;
    case "right":
      clickType = 2;
      break;
    default:
      clickType = 0;
  }
  
  await ele.click({ button: clickType });
//   await browser.pause(30000);
};

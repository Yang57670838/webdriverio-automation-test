import chai from "chai";

// assume will be a mui <Select /> component
export default async (datatable) => {
    const argArray = await datatable.hashes();
    let options = await $('.MuiMenu-list').$$('li[class*="MuiMenuItem-root"]');
    console.log("options", options);
    for (let i = 0; i < argArray.length; i++) {
        let ele = options[i];
        let value = await ele.getText();
        chai.expect(value).to.equal(argArray[i].Option);
    }
}

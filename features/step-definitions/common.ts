import { Given, When, Then } from '@wdio/cucumber-framework';
import chai from "chai";
import enterInputField from "../helper/enterInputField.js";
import selectFieldSelect from "../helper/selectFieldSelect.js";
import clickIntoElement from "../helper/clickIntoElement.js";
import validateSelectOpenOptions from "../helper/validateSelectOpenOptions.js";
import checkSingleCheckbox from "../helper/checkSingleCheckbox.js";
import logger from "../helper/logger.js";

import LoginPage from '../pageobjects/login.page.js';
import SecurePage from '../pageobjects/secure.page.js';
import DashboardPage from '../pageobjects/dashboard.page.js';
import gstreportPage from '../pageobjects/gstreport.page.js';
import HistoryDetailsPage from '../pageobjects/historyDetails.page.js';

const pages = {
    login: LoginPage,
    dashboard: DashboardPage,
    gstreport: gstreportPage,
    historyDetails: HistoryDetailsPage,
}

Given(/^I am on the (\w+) page$/, async function (page) {
    logger.info(`page: ${page}`)
    await pages[page].open();
    await browser.setTimeout({ implicit: 15000, pageLoad: 10000 });
    await browser.maximizeWindow();
    this.currentPage = page;
    // await browser.pause(5000);
    // await browser.debug();
});

Given(/^I pause for (\d+) ms$/, async function (ms) {
    await browser.pause(ms);
});

Then(/^I see page header as (.*)$/, async function (header) {
    const page = this.currentPage;
    let pageHeader = await pages[page].pageHeader.getText()
    chai.expect(pageHeader).to.equals(header);
});

When(/^I login with (\w+) and (.+)$/, async (username, password) => {
    await LoginPage.login(username, password)
});

Then(/^I should see a flash message saying (.*)$/, async (message) => {
    await expect(SecurePage.flashAlert).toBeExisting();
    await expect(SecurePage.flashAlert).toHaveTextContaining(message);
});

Then(/^I should see the menu item (.*) with text (.*)$/, async (menuIndex, menuTxt) => {
    await expect(DashboardPage.leftManuBarList[menuIndex].$('a')).toBeExisting();
    await expect(DashboardPage.leftManuBarList[menuIndex].$('a')).toHaveText(menuTxt);
});

When(/^I enter (.*) into text field (.+) with method (.+)$/, enterInputField);

When(/^I (left|middle|right) click on field (.*)$/, clickIntoElement);

Then(/^I should see the options inside current opening dropdown select$/, validateSelectOpenOptions);

When(/^I check the page checkbox$/, checkSingleCheckbox);

Then(/^I should see (.*) rows in the historyDetails page grid table$/, async (rowNo) => {
    const rows = await HistoryDetailsPage.tableRows;
    const columns = await HistoryDetailsPage.tableHeaders;
    chai.expect(rows.length).to.equal(Number(rowNo))
    chai.expect(columns.length).to.equal(Number(rowNo))
    // TODO: can use a loop to go through each headers and cells text and assert
    let firstCell = await (await rows[0].$('td')).getText();
    let firstHeader = await columns[0].getText();
    chai.expect(firstHeader).to.equals("Dessert (100g serving)");
    chai.expect(firstCell).to.equals("159");
});




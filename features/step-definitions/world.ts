import { setWorldConstructor } from "@wdio/cucumber-framework";
import chai from "chai";

// share data between steps in one Scenario
class CustomWorld {
    currentPage: string;
    constructor() {
        this.currentPage = "";
    }
}
setWorldConstructor(CustomWorld)
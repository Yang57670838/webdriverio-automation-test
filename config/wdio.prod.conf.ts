import fs from 'node:fs/promises'
import { generate } from 'multiple-cucumber-html-reporter'
import cucumberJson from 'wdio-cucumberjs-json-reporter'
import { config as baseConfig } from '../wdio.conf.js'

const overrides = {
    baseUrl: 'https://...',
    reporters: [
        'spec',
        [
            'cucumberjs-json',
            {
                jsonFolder: '.tmp/json/',
                language: 'en',
            },
        ],
    ],
    onPrepare: function (config, capabilities) {
        return fs.rm('.tmp/', { recursive: true });
    },
    afterStep: async function (step, scenario, result, context) {
        if (!result.passed) {
            cucumberJson.attach(await browser.takeScreenshot(), 'image/png');
        }
    },
    onComplete: function(exitCode, config, capabilities, results) {
        generate({
            jsonDir: '.tmp/json/',
            reportPath: '.tmp/report/',
          });
    },
};

export const config = Object.assign(baseConfig, overrides);
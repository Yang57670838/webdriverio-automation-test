import { config as baseConfig } from '../wdio.conf.js'

const overrides = {
    baseUrl: 'http://localhost:3000'
};

export const config = Object.assign(baseConfig, overrides);